import React, {Component} from 'react';
import { View, StyleSheet, Image, Text, ScrollView } from 'react-native';

import BgImage from '../images/bomb.jpg';
import MovieLabel from '../component/MovieLabel';
import { PrimaryColor, SecondaryColor } from '../component/ThemeColors';

class HomeScreen extends Component{
    render() {
        return (
            <View style={styles.containerStyle}>

                <Image style={styles.bgImageStyle} source={BgImage}/>
               
                <View style={styles.list}>
                    <Text style={styles.textList}>LIST MOVIE</Text>
                </View>

                <ScrollView>
                    <MovieLabel/>
                </ScrollView>
            
            </View>
        )
    }
}

const styles = StyleSheet.create({
    containerStyle: {
        flex:1,
    },
    bgImageStyle: {
        flex:1,
        resizeMode: 'cover',
        alignSelf:'center',
        justifyContent: 'center',
        alignContent: 'center',
        position: 'absolute',
        width: '105%',
        height: '100%'
    },list: {
        marginVertical: 20,
        alignSelf:'center',
        backgroundColor:SecondaryColor,
        padding:20,
        borderTopStartRadius:20,
        borderTopEndRadius:20,
        borderBottomColor:PrimaryColor,
        borderBottomWidth:8
    },
    textList: {
        fontSize: 26,
        fontWeight:'bold',
        color: PrimaryColor
    },
})

export default HomeScreen;